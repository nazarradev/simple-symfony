<?php

namespace App\Controller;

use App\Form\CarType;
use App\Repository\CarRepository;
use phpDocumentor\Reflection\Types\This;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Car;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class CarController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(CarRepository $carRepository ): Response
    {
        $cars = $carRepository->findAll();

        return $this->render('car/index.html.twig', [
            'cars' => $cars,
            'sort' => null
        ]);

    }

    /**
      * @Route("/addCar/", name="addCar")
      */

    public function create(Request $request)
    {

        $car = new Car();

        $form = $this->createForm(CarType::class, $car);

        $form->handleRequest($request);


        if($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();;

            $em->persist($car);

            $em->flush();

            return $this->render('car/create.html.twig', [
                'form' => $form->createView()
            ]);
        }

        return $this->render('car/create.html.twig', [
            'form' => $form->createView()

        ]);

    }

    /**
     * @Route("/edit/{id}", name="edit")
     */

    public function edit(Car $car, Request $request, CarRepository $carRepository  ){

        $car = $cars = $carRepository->find($car);;

        $form = $this->createForm(CarType::class, $car);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();;

            $em->persist($car);

            $em->flush();
        }

        return $this->render('car/create.html.twig', [
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("/delete/{id}", name="delete")
     */

    public function delete(Car $car){
        $em = $this ->getDoctrine()->getManager();

        $em -> remove($car);

        $em -> flush();;

        return $this -> redirect($this->generateUrl('index'));
    }


    /**
     * @Route("/sorting{sort}", name="sorting")
     */

    public function sorting(CarRepository $carRepository, $sort=null){

        $cars = $carRepository->findAll();

        switch ($sort){
            case null:

                return $this->render('car/index.html.twig', [
                    'cars' => $cars,
                    'sort' => $sort
                ]);

            case 'alphabetically':

                usort($cars, function ($a, $b)
                {
                    return strnatcmp($a->getMark(), $b->getMark());
                });

                return $this->render('car/index.html.twig', [

                    'cars' => $cars,
                    'sort' => $sort

                ]);

            case 'alphabetically_reverse':

                usort($cars, function ($a, $b)
                {
                    return strnatcmp($b->getMark(), $a->getMark());
                });

                return $this->render('car/index.html.twig', [

                    'cars' => $cars,
                    'sort' => $sort

                ]);

            case 'numeric':

                usort($cars, function ($a, $b)
                {
                    return ucwords($a->getMade()) > ucwords($b->getMade());
                });

                return $this->render('car/index.html.twig', [

                    'cars' => $cars,
                    'sort' => $sort

                ]);

            case 'numeric_reverse':

                usort($cars, function ($a, $b)
                {
                    return ucwords($a->getMade()) < ucwords($b->getMade());
                });
                return $this->render('car/index.html.twig', [

                    'cars' => $cars,
                    'sort' => $sort
                ]);

        }
    }


}



