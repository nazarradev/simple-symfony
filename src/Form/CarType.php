<?php

namespace App\Form;

use App\Entity\Car;
use phpDocumentor\Reflection\Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class CarType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('mark',TextType::class,[
                'attr' => [
                    'placeholder' => 'Fiat',
                ]
                ])
            ->add('model',TextType::class,[
                'attr' => [
                    'placeholder' => '126p',
                ]
            ])
            ->add('made',IntegerType::class,[
                'attr' => [
                    'placeholder' => '2021',
                ]
            ])
            ->add('Submit', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Car::class,
        ]);
    }
}
