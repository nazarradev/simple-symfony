<?php

namespace App\Entity;

use App\Repository\CarRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\LengthValidator;

/**
 * @ORM\Entity(repositoryClass=CarRepository::class)
 */
class Car
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Regex(
     *     pattern = "/[A-Z]{1,}[a-zA-z]{2,}/",
     *     match = true,
     *     message = "The mark must start with a big letter and contain at least 3 letters",
     * )
     *
     */
    private $mark;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *      min = 3,
     *      minMessage = "The model must be at least 3 characters long",
     *
     * )
     */
    private $model;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Length(
     *      min = 4,
     *      max = 4,
     *      minMessage = "This value should have exactly 4 characters",
     *      maxMessage = "This value should have exactly 4 characters"
     * )
     */
    private $made;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMark(): ?string
    {
        return $this->mark;
    }

    public function setMark(string $mark): self
    {
        $this->mark = $mark;

        return $this;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function setModel(string $model): self
    {
        $this->model = $model;

        return $this;
    }

    public function getMade(): ?int
    {
        return $this->made;
    }

    public function setMade(int $made): self
    {
        $this->made = $made;

        return $this;
    }
}
